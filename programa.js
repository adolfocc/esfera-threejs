//Escena: Disposición de elemntos para ser mostrados 

//1. Crear escena
//Three es el nombre de la clase que representa la biblioteca

let escena= new THREE.Scene()

//2. Crear un objeto cámara

let resolucion= window.innerWidth/window.innerHeight
let camara= new THREE.PerspectiveCamera(30, resolucion, 1, 100)
camara.position.z=5
//3. Crear un objeto que nos ayude a dibujar la escena en la pantalla (renderizar)

let lienzo= new THREE.WebGLRenderer()
lienzo.setSize(window.innerWidth,window.innerHeight)
//Etiqueta canvas
document.body.appendChild(lienzo.domElement)

//4. Crear los objeos que perteneceran a la escena
//Geometría: Un objeto que representa un elmento geométrico (líneas, puntos, circulos, rectángulos, tríangulos...)

let geometriaBase= new THREE.SphereGeometry()

//Material: Recubrimiento de una geometría

let material= new THREE.MeshBasicMaterial({color: 0x0000FF, wireframe:true })

//Malla (mesh): La representacíon de una geometría. Las mallas son las que se pueden renderizar 
let miEsfera= new THREE.Mesh(geometriaBase, material)
escena.add(miEsfera)

//Crear animación

let animar=function(){

    requestAnimationFrame(animar)

    miEsfera.rotation.y= miEsfera.rotation.y+0.01
    miEsfera.rotation.x= miEsfera.rotation.x+0.01
    lienzo.render(escena,camara)
}

//5. Renderizar la escena

animar()


